package com.digitalml.rest.resources.codegentest.service.InvoiceAdaptor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.InvoiceAdaptorService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Invoice Adaptor
 * Access and update of sample information from CSV adaptor.
 *
 * @author admin
 * @version 2
 *
 */

public class InvoiceAdaptorServiceSandboxImpl extends InvoiceAdaptorService {
	
    /**
    * The logic implementation for getInvoiceDetailsProcessStep1
    *
    */
	public GetInvoiceDetailsCurrentStateDTO getInvoiceDetailsProcessStep1(GetInvoiceDetailsCurrentStateDTO currentState) {
		
		// Add code here to fulfil the "logic" step
		
		
		return currentState;
	}

    public GetInvoiceDetailsCurrentStateDTO getInvoiceDetailsUseCaseStep0(GetInvoiceDetailsCurrentStateDTO currentState) {
    

        GetInvoiceDetailsReturnStatusDTO returnStatus = new GetInvoiceDetailsReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of sample information from CSV adaptor.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetInvoiceDetailsCurrentStateDTO getInvoiceDetailsUseCaseStep1(GetInvoiceDetailsCurrentStateDTO currentState) {
    

        GetInvoiceDetailsReturnStatusDTO returnStatus = new GetInvoiceDetailsReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of sample information from CSV adaptor.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetInvoiceDetailsCurrentStateDTO getInvoiceDetailsUseCaseStep2(GetInvoiceDetailsCurrentStateDTO currentState) {
    

        GetInvoiceDetailsReturnStatusDTO returnStatus = new GetInvoiceDetailsReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of sample information from CSV adaptor.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetInvoiceDetailsCurrentStateDTO getInvoiceDetailsUseCaseStep3(GetInvoiceDetailsCurrentStateDTO currentState) {
    

        GetInvoiceDetailsReturnStatusDTO returnStatus = new GetInvoiceDetailsReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of sample information from CSV adaptor.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = InvoiceAdaptorService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}