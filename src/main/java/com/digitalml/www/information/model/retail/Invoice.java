package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Invoice:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    }
  }
}
*/

public class Invoice {

	@Size(max=1)
	private String id;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
}