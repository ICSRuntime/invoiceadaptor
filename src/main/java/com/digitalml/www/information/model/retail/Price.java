package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Price:
{
  "type": "object",
  "properties": {
    "amount": {
      "type": "number",
      "format": "float"
    },
    "type": {
      "type": "object",
      "properties": {
        "code": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "currencyCode": {
      "type": "string"
    }
  }
}
*/

public class Price {

	@Size(max=1)
	private Float amount;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Type type;

	@Size(max=1)
	private String currencyCode;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    type = null;
	    currencyCode = null;
	}
	public Float getAmount() {
		return amount;
	}
	
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public com.digitalml.www.information.model.retail.Type getType() {
		return type;
	}
	
	public void setType(com.digitalml.www.information.model.retail.Type type) {
		this.type = type;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}