<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8" version="1.0"/>
			
	<xsl:template name="Call Adaptor" mode="transformation">
<!--
	Call Invoice Adaptor Canonical Cache

	Transforms the classes:
		com.digitalml.rest.resources.codegentest.service.InvoiceAdaptorService.GetInvoiceDetailsInputParametersDTO
		com.digitalml.rest.resources.codegentest.service.InvoiceAdaptorService.GetInvoiceDetailsReturnDTO
-->
			
	
		<Response><id>
			<xsl:value-of select="id"/>
		</id></Response>
	
	
		<Response><supplier>
			<xsl:value-of select=""/>
		</supplier></Response>
	
	
		<Response><method>
			<xsl:value-of select=""/>
		</method></Response>
	
	
		<Response><status>
			<xsl:value-of select=""/>
		</status></Response>
	
	
		<Response><date>
			<xsl:value-of select=""/>
		</date></Response>
	
	
		<Response><currency>
			<xsl:value-of select=""/>
		</currency></Response>
	
	
		<Response><items><id>
			<xsl:value-of select=""/>
		</id></items></Response>
	
	
		<Response><items><description>
			<xsl:value-of select=""/>
		</description></items></Response>
	
	
		<Response><items><cost><amount>
			<xsl:value-of select=""/>
		</amount></cost></items></Response>
	
	
		<Response><items><cost><type><code>
			<xsl:value-of select=""/>
		</code></type></cost></items></Response>
	
	
		<Response><items><cost><type><description>
			<xsl:value-of select=""/>
		</description></type></cost></items></Response>
	
	
		<Response><items><cost><currencyCode>
			<xsl:value-of select=""/>
		</currencyCode></cost></items></Response>
	
	
		<Response><items><status>
			<xsl:value-of select=""/>
		</status></items></Response>
	
	
		<Response><items><supplier>
			<xsl:value-of select=""/>
		</supplier></items></Response>
	
	
		<Response><items><sku>
			<xsl:value-of select=""/>
		</sku></items></Response>
	
	
		<Response><items><type>
			<xsl:value-of select=""/>
		</type></items></Response>
	
	
		<Response><items><price><amount>
			<xsl:value-of select=""/>
		</amount></price></items></Response>
	
	
		<Response><items><price><type><>
			<xsl:value-of select=""/>
		</></type></price></items></Response>
	
	
		<Response><items><price><type><description>
			<xsl:value-of select=""/>
		</description></type></price></items></Response>
	
	
		<Response><items><price><currencyCode>
			<xsl:value-of select=""/>
		</currencyCode></price></items></Response>
	
	
		<Response><items><weight>
			<xsl:value-of select=""/>
		</weight></items></Response>
	

	</xsl:template>

	
</xsl:stylesheet>