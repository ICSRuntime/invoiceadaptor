package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class InvoiceAdaptorTests {

	@Test
	public void testResourceInitialisation() {
		InvoiceAdaptorResource resource = new InvoiceAdaptorResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetInvoiceDetailsNoSecurity() {
		InvoiceAdaptorResource resource = new InvoiceAdaptorResource();
		resource.setSecurityContext(null);

		Response response = resource.getInvoiceDetails(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}